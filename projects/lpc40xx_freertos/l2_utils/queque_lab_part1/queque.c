#include "queque.h"

void queue__init(queue_s *queue) {
  queue->head = 0;
  queue->tail = 0;
  queue->count = 0;
}

bool queue__push(queue_s *queue, uint8_t push_value) {
  if (queue->count < sizeof(queue->queue_memory)) {
    queue->queue_memory[queue->tail] = push_value;
    queue->tail = (queue->tail + 1) % sizeof(queue->queue_memory);
    queue->count++;
    return true;
  }
  return false;
}

bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  if (queue->count > 0) {
    *pop_value = queue->queue_memory[queue->head];
    queue->head = (queue->head + 1) % sizeof(queue->queue_memory);
    queue->count--;
    return true;
  }
  return false;
}

size_t queue__get_item_count(const queue_s *queue) { return queue->count; }