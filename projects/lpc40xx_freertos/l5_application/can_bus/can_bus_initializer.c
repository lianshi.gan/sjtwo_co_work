#include "can_bus_initializer.h"
#include "can_bus.h"
#include <stddef.h>

static const can__num_e can_bus_1 = can1;
void can__initializer(void) {
  // TODO: You should refactor this initialization code to dedicated
  // "can_bus_initializer.h" code module Read can_bus.h for more details
  can__init(can_bus_1, 200, 100, 100, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can_bus_1);
}