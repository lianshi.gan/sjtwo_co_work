#include "can_bus_message_handler.h"
#include "board_io.h"
#include "can_bus.h"
#include <stdio.h>
#include <string.h>

static const can__num_e can_bus_1 = can1;

static void can__tx_send_message(void) {

  gpio_s switch_gpio = board_io__get_sw1();
  bool switch_pressed = gpio__get(switch_gpio);

  can__msg_t msg = {};
  msg.msg_id = 0x123;
  msg.frame_fields.data_len = 1;
  msg.data.bytes[0] =
      switch_pressed ? 0xAA : 0x00; // 0xAA if pressed, 0x00 otherwise

  if (!can__tx(can_bus_1, &msg, 0)) {
    printf("Failed to transmit message\n");
  }
}

static void can__rx_receive_messages(void) {
  can__msg_t msg;

  while (true) {
    memset(&msg, 0, sizeof(msg));

    if (!can__rx(can_bus_1, &msg, 0)) {
      break;
    }

    printf("received id: %ld \n", msg.msg_id);
    printf("received data: %d \n", msg.data.bytes[0]);
    // Process the received message
    if (msg.msg_id == 0x123 && msg.frame_fields.data_len == 1) {
      gpio_s led_gpio = board_io__get_led3();
      if (msg.data.bytes[0] == 0xAA) {
        gpio__set(led_gpio);
      } else {
        gpio__reset(led_gpio);
      }
    }
  }
}

void can_handler__run_once(void) {
  // can__tx_send_message();

  can__rx_receive_messages();
}