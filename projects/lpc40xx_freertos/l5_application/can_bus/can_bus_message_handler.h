
#pragma once

#include "can_bus.h"
#include <stdbool.h>
#include <stdint.h>

void can_handler__run_once(void);
