#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"

#include "can_bus_message_handler.h"
#include "unity.h"
#include <stdio.h>
#include <string.h>

void setUp(void) {}
void tearDown(void) {}

// void test_can_handler_board_A_sends_correct_message_for_switch_pressed(void)
// {
//   gpio_s switch_gpio = {.port_number = 1, .pin_number = 0};
//   board_io__get_sw1_ExpectAndReturn(switch_gpio);

//   gpio__get_ExpectAndReturn(switch_gpio, true);

//   can__msg_t expected_msg = {
//       .msg_id = 0x123, .frame_fields.data_len = 1, .data.bytes[0] = 0xAA};
//   can__tx_ExpectAndReturn(can1, &expected_msg, 0, true);

//   can_handler__run_once();
// }

void test_can_handler_board_B_processes_received_message(void) {
  can__msg_t simulated_receive_message = {
      .msg_id = 0x123, .frame_fields.data_len = 1, .data.bytes[0] = 0xAA};

  // Setup mock expectation for can__rx with a return through pointer
  // behavior
  gpio_s switch_gpio = {.port_number = 3, .pin_number = 0};
  board_io__get_led3_ExpectAndReturn(switch_gpio);
  gpio__set_Expect(switch_gpio);
  can__rx_ExpectAndReturn(can1, NULL, 0, true);

  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&simulated_receive_message);

  can__msg_t msg;
  memset(&msg, 0, sizeof(msg));
  can__rx_ExpectAndReturn(can1, &msg, 0, false);

  can_handler__run_once();
}
