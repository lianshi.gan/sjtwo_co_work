
#include "gpio.h"

// Initializes
void switch_led_logic__initialize(void);

// Reads the switch state and set LED
void switch_led_logic__run_once(void);

// Get switch state only
bool switch_led_logic__get_switch_state(void);
