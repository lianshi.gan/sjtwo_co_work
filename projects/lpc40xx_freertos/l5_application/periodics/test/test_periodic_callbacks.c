#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as
// board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockcan_bus_initializer.h"
#include "Mockgpio.h"

#include "Mockcan_bus_message_handler.h"

/**
 * Generate "Mocks" for these files
 */
#include "Mockswitch_led_logic.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

// Add expect during the periodic_callbacks__initialize() function
void test__periodic_callbacks__initialize(void) {
  switch_led_logic__initialize_Expect();

  can__initializer_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__100Hz(void) {
  can_handler__run_once_Expect();
  gpio_s gpio = {};
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);
  periodic_callbacks__100Hz(0);
}
